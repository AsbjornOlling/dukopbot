{ pkgs ? import <nixpkgs> {} }:

let
  pyenv = pkgs.python311.withPackages (ps: (import ./requirements.nix) { pypkgs = ps; } );
in
pkgs.mkShell {
    buildInputs = [pkgs.python311 pyenv];
}
