{ pypkgs ? import <nixpkgs> {} }:
[
    pypkgs.httpx
    pypkgs.feedparser
    pypkgs.mastodon-py
    pypkgs.pydantic
    pypkgs.email-validator
    pypkgs.pyyaml
    pypkgs.dateutil
]
