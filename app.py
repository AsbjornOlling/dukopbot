#!/usr/bin/env python3

"""
This is a mastodon bot which posts events from the dukop.dk RSS feeds.

It's completely stateless.
It works something like this:

Run on an interval (ca. 1 hour)
    Read dukop.dk RSS feeds (gives the next 30 events).
    Read the last 100 toots by the bot.
    Correllate toots and rss feed events by comparing their http links to dukop.dk
    Post new toots for any new events.
    Toot any new, un-tooted events
    Edit the toot if exists, but the text is different.
"""

# std lib
import re
import os
import asyncio
import functools
import multiprocessing
from pathlib import Path
from zoneinfo import ZoneInfo
from typing import NewType, Any
from dataclasses import dataclass
from datetime import datetime, timedelta

# 3rd party
import time
import yaml
import httpx
import feedparser
import dateutil.parser
from mastodon import Mastodon
from pydantic.dataclasses import dataclass
from pydantic import HttpUrl, EmailStr, SecretStr, validator


@dataclass(frozen=True)
class BotConfig:
    mastodon_username: str
    mastodon_base_url: HttpUrl
    mastodon_client_id: str
    mastodon_account_email: EmailStr
    mastodon_account_password: SecretStr
    dukop_feed_url: HttpUrl
    poll_interval_seconds: timedelta = timedelta(seconds=3600)


@dataclass(frozen=True)
class RSSEvent:
    id: HttpUrl
    title: str
    link: HttpUrl
    summary: str  # HTML
    author: str | None
    start_datetime: datetime

    @validator("start_datetime")
    def parse_datetime(cls, datestr: str):
        # 3rd party magic parser 🪄
        return dateutil.parser.parse(datestr)

    @validator("author")
    def parse_author(cls, author: str):
        if author in ("Unspecified host", "Unnamed user"):
            return None
        return author


@dataclass(frozen=True)
class Toot:
    id: HttpUrl
    content: str


def main(config: BotConfig) -> None:
    mastodon = connect_mastodon(config)
    while True:
        # wait a fixed interval
        time.sleep(config.poll_interval_seconds / timedelta(seconds=1))

        # get events from dukop
        rss: dict[HttpUrl, RSSEvent] = get_dukop_rss_events(config)

        # read radikal.social feed
        toots: dict[HttpUrl, Toot] = get_toots(config, mastodon)

        # correlate toots and dukop RSS events
        edits = {key for key in toots.keys() & rss.keys() if toots[key] != rss[key]}

        # rss events which have not been tooted about are new events
        new_events = rss.keys() - toots.keys()

        # test assumptions
        assert edits & new_events == set(), "Edits and new events are not distinct"

        # TODO [MSAPI] send edits to mastodon
        # TODO [MSAPI] send new events to mastodon


def get_dukop_rss_events(config: BotConfig) -> dict[HttpUrl, RSSEvent]:
    """Get RSS from Dukop.dk and parse into a dict of RSSEvents"""
    # read dukop.dk rss
    r = httpx.get(config.dukop_feed_url)
    r.raise_for_status()

    # parse dukop.dk events
    rss_dict: feedparser.util.FeedParserDict = feedparser.parse(r.text)
    rss_events = [RSSEvent(**d) for d in rss_dict["entries"]]

    # useful dict
    rss: dict[HttpUrl, RSSEvent] = {re.link: re for re in rss_events}

    # check assumptions, all links should be unique
    assert len(rss) == len(rss_events)

    return rss


async def daily_worker():
    """Sleep until 18:00
    Post all events that are happening today.
    """
    # TODO calculate time until 18:00
    # TODO get ICS feed, find all events for today
    # TODO make toot with titles, links and times
    raise NotImplemented


@functools.cache
def connect_mastodon(config: BotConfig) -> Mastodon:
    """Authenticate with Mastodon.
    Return a Mastodon.py client object.
    """
    client_secret_file = Path(f"{config.mastodon_username}.clientsecret")
    if not client_secret_file.exists():
        # create a client if we haven't already
        Mastodon.create_app(
            config.mastodon_client_id,
            api_base_url=config.mastodon_base_url,
            to_file=client_secret_file,
        )

    # load file and create client object
    mastodon = Mastodon(client_id=client_secret_file)

    # now authenticate as user
    user_secret_file = Path(f"{config.mastodon_username}.usersecret")
    mastodon.log_in(
        config.mastodon_account_email,
        config.mastodon_account_password,
        to_file=user_secret_file,
    )
    return mastodon


def get_toots(
    config: BotConfig, mastodon: Mastodon, n: int = 1000
) -> dict[HttpUrl, Toot]:
    """Gets the most recent `n` toots by us
    `n` should be large enough to comfortably fit all scheduled future events
    May return fewer than `n` posts.
    """
    statuses: list[dict[str, Any]] = []
    maxid: int | None = None
    account_data = mastodon.account_lookup(config.mastodon_username)
    account_id = account_data["id"]
    while True:
        # fetch statuses
        statuses_batch = mastodon.account_statuses(
            account_id,
            # max is 40, see: https://docs.joinmastodon.org/methods/accounts/#statuses
            limit=40,
            maxid=maxid,
        )

        if len(statuses_batch) == 0:
            # didn't reach target, but we read all the toots
            break

        # start reading from the last id on next fetch
        maxid = statuses_batch[-1]["id"]

        if len(statuses) >= n:
            # target reached
            break

    # fetch plaintext toots status_dicts
    # ...because the html formatted content kind of sucks.
    status_sources: list[dict] = [mastodon.status_source(d["id"]) for d in statuses]

    # parse into toot objects
    toots: dict[HttpUrl, Toot] = {
        t.id: t for d in status_sources if (t := parse_toot(d))
    }

    # we're done
    return toots


def parse_toot(status_source) -> Toot | None:
    """Parse a status_dict from Mastodon.py into a Toot object"""
    # look for a dukop.dk url in the toot html
    regex_pattern = r"https://dukop.dk/en/event/[0-9]+/"
    match = re.match(regex_pattern, status_source)
    if not match:
        return None
    url = match.group(0)

    # return Toot
    # TODO: media attachments
    return Toot(id=url, content=status_source)


def toot_from_rss_event(rss_event: RSSEvent) -> Toot:
    """Format a toot from an RSS event
    The toot includes:
     - Date
     - Title
     - Link
     - Image
     - Something correllable with RSS
    Does not send the event.
    """
    # try to find datetime in rss entry content
    # because there is no "end_datetime" field, this is the only way to get end time
    regex = (
        r"When:.+"
        r"(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)"
        r" (January|February|March|April|May|June|July|August|September|October|November|December)"
        r" \d{1,2}"
        r" at \d\d:\d\d - \d\d:\d\d"
    )
    match = re.search(regex, rss_event.summary)
    datestring: str | None = (
        # clean up string
        match.group(0).removeprefix("When:").strip()
        if match
        # fall back to rss datetime field if regex fails...
        else f"{rss_event.start_datetime:%A %B %d at %H:%M}"
    )

    # make the object!
    return Toot(
        id=rss_event.id,
        content=f"""{rss_event.title}
{datestring}
{rss_event.link}""",
        # TODO: image
    )


if __name__ == "__main__":
    # find config file
    configfile = Path(os.getenv("DUKOPBOT_CONFIG", "./botconfig.yaml"))
    if not configfile.exists():
        raise EnvironmentError(f"File {configfile} does not exist.")

    # parse config with pyyaml...
    with open(configfile) as yamlstr:
        yamldata: dict = yaml.safe_load(yamlstr)
    # ...and pydantic
    botconfigs = [BotConfig(**d) for d in yamldata["bots"]]

    # run N processes, one for each bot in the config
    pool = multiprocessing.Pool(len(botconfigs))
    pool.map(main, botconfigs)
