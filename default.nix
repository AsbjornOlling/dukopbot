{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/da0b0bc6a5d699a8a9ffbf9e1b19e8642307062a.tar.gz") {} }:
let
  pyenv = pkgs.python311.withPackages (ps: (import ./requirements.nix) { pypkgs = ps; } );
in
pkgs.dockerTools.buildImage {
  name = "dukopbot";
  copyToRoot = pkgs.buildEnv {
    name = "image-root";
    paths = with pkgs; [
      bash
      coreutils
      pyenv
    ];
    pathsToLink = [ "/bin" ];
  };
  config = {
    Cmd = ["${pyenv}/bin/python" ./app.py];
  };
}
